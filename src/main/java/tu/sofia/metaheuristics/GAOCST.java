package tu.sofia.metaheuristics;

import java.util.Arrays;

public class GAOCST {
    
//    public static int[][] 
//            requirements = {
//                {0, 2, 8, 7, 3},
//                {2, 0, 4, 1, 1},
//                {8, 4, 0, 2, 3},
//                {7, 1, 2, 0, 5},
//                {3, 1, 3, 5, 0}
//            },
//            distances = {   
//                {0, 7, 4, 1, 2}, 
//                {7, 0, 5, 8, 3}, 
//                {4, 5, 0, 3, 2}, 
//                {1, 8, 3, 0, 2}, 
//                {2, 3, 2, 2, 0}
//            };
    
    public static int[][] 
            requirements = {
                {0, 2, 8, 7, 3, 3},
                {2, 0, 4, 1, 1, 5},
                {8, 4, 0, 2, 3, 2},
                {7, 1, 2, 0, 5, 4},
                {3, 1, 3, 5, 0, 4},
                {3, 5, 2, 4, 4, 0}
            },
            distances = {   
                {0, 7, 4, 1, 2, 3}, 
                {7, 0, 5, 8, 3, 1}, 
                {4, 5, 0, 3, 2, 3}, 
                {1, 8, 3, 0, 2, 5}, 
                {2, 3, 2, 2, 0, 1},
                {3, 1, 3, 5, 1, 0}
            };

	public static void main(String[] args) {
		
		// Размер на популацията
		final int populationSize = 10;
		
		// Максималният брой поколения за симулацията.
		final int maxGenerations = 10;
		
		// Вероятността за кръстосване между кои да са членове на попуацията
		// където 0.0 <= crossoverRatio <= 1.0
		final float crossoverRatio = 0.8f;
		
		// Частта от популацията, която да остане непроменена между еволюциите
		// където 0.0 <= elitismRatio < 1.0
		final float elitismRatio = 0.1f;
		
                // Вероятността от някой от членовете на популацията да мутира
		// където 0.0 <= mutationRatio <= 1.0
		final float mutationRatio = 0.5f;
	
                long startTime = System.currentTimeMillis();
		
		// Създаване на началната популация
		Population pop = new Population(populationSize, crossoverRatio, 
				elitismRatio, mutationRatio);

                // Стартираме еволюцията на популацията, спираме когато 
                // достигнем максималния брой поколения или когато намерим решение.
		int i = 0;
		Chromosome best = pop.getPopulation()[0];
		
		while ((i++ <= maxGenerations) && (best.getFitness() != 0)) {
			System.out.println("Generation " + i + ": " + best.getFitness());
			pop.evolve();
			best = pop.getPopulation()[0];
		}
		
		long endTime = System.currentTimeMillis();
		
		// Пишем информация в конзолата
		System.out.println("Generation " + i + ": " + best.getFitness());
		System.out.println("Total execution time: " + (endTime - startTime) + "ms");
	}
}