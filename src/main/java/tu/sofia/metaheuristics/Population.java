package tu.sofia.metaheuristics;

import java.util.Arrays;
import java.util.Random;

public class Population {

	private static final int TOURNAMENT_SIZE = 3;
	
	private static final Random rand = new Random(System.currentTimeMillis());

	private float elitism;
	private float mutation;
	private float crossover;
	private Chromosome[] popArr;

	public Population(int size, float crossoverRatio, float elitismRatio, 
			float mutationRatio) {
		
		this.crossover = crossoverRatio;
		this.elitism = elitismRatio;
		this.mutation = mutationRatio;
		
		// Генерираме начална популация
		this.popArr = new Chromosome[size];
		for (int i = 0; i < size; i++) {
			this.popArr[i] = Chromosome.generateRandom();
		}

		Arrays.sort(this.popArr);
	}

	public void evolve() {
		// Създаваме буфер за следващото поколение
		Chromosome[] buffer = new Chromosome[popArr.length];
		
		// Копираме част от поколонието непроменено, в зависимост коефициента на елитизъм
		int idx = Math.round(popArr.length * elitism);
		System.arraycopy(popArr, 0, buffer, 0, idx);

		// Итерираме по останалите елементи от популацията и еволюираме.
		while (idx < buffer.length) {
			// Правим проверка дали ще има кръстосване
			if (rand.nextFloat() <= crossover) {
				
				// Избираме родителите и ги кръстосваме, за да получим децата
				Chromosome[] parents = selectParents();
				Chromosome[] children = parents[0].mate(parents[1]);
				
				// Проверяваме дали първото дете трябва да се мутира
				if (rand.nextFloat() <= mutation) {
					buffer[idx++] = children[0].mutate();
				} else {
					buffer[idx++] = children[0];
				}
				
				// Повтаряме проверката за второто дете
				if (idx < buffer.length) {
					if (rand.nextFloat() <= mutation) {
						buffer[idx] = children[1].mutate();
					} else {
						buffer[idx] = children[1];
					}
				}
			} else { // Няма кръстосване, затова копираме
				// Определяме дали мутацията е нужна
				if (rand.nextFloat() <= mutation) {
					buffer[idx] = popArr[idx].mutate();
				} else {
					buffer[idx] = popArr[idx];
				}
			}
			
			// Увеличаваме брояча
			++idx;
		}

		// Сортираме буфера в зависимост от фитнес функцията.
		Arrays.sort(buffer);
		
		// Рестартираме популацията
		popArr = buffer;
	}
	
	public Chromosome[] getPopulation() {
		Chromosome[] arr = new Chromosome[popArr.length];
		System.arraycopy(popArr, 0, arr, 0, popArr.length);
		
		return arr;
	}
	
	public float getElitism() {
		return elitism;
	}

	public float getCrossover() {
		return crossover;
	}

	public float getMutation() {
		return mutation;
	}

	private Chromosome[] selectParents() {
		Chromosome[] parents = new Chromosome[2];

		// На случаен принцип избираме 2 родителя чрез турнир
		for (int i = 0; i < 2; i++) {
			parents[i] = popArr[rand.nextInt(popArr.length)];
			for (int j = 0; j < TOURNAMENT_SIZE; j++) {
				int idx = rand.nextInt(popArr.length);
				if (popArr[idx].compareTo(parents[i]) < 0) {
					parents[i] = popArr[idx];
				}
			}
		}
		
		return parents;
	}
}