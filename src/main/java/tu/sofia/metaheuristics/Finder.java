package tu.sofia.metaheuristics;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author lazar
 */
public class Finder {

    private static List<Node> constructPath(Node node) {
        LinkedList path = new LinkedList();
        while (node.parent != null) {
            path.addFirst(node);
            node = node.parent;
        }
        path.add(node);
        return path;
    }

    public static List search(boolean[][] gene, Node startNode, Node goalNode) {
        // list of visited nodes
        LinkedList closedList = new LinkedList();

        // list of nodes to visit (sorted)
        LinkedList openList = new LinkedList();
        openList.add(startNode);
        startNode.parent = null;

        while (!openList.isEmpty()) {
            Node node = (Node) openList.removeFirst();
            if (node.equals(goalNode)) {
                // path found!
                return constructPath(node);
            } else {
                closedList.add(node);

                // add neighbors to the open list
                for (int i = 0; i < gene.length; i++) {
                    if (gene[node.x][i]) {
                        Node neighborNode = new Node(i);
                        if (!closedList.contains(neighborNode) && !openList.contains(neighborNode)) {
                            neighborNode.parent = node;
                            openList.add(neighborNode);
                        }
                    }
                }
            }
        }

        return null;
    }
}
