package tu.sofia.metaheuristics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;

public class Chromosome implements Comparable<Chromosome> {

    private final boolean[][] gene;
    private final int fitness;

    private static final Random rand = new Random(System.currentTimeMillis());

    public Chromosome(boolean[][] gene) {
        this.gene = gene;
        this.fitness = calculateFitness(gene);
    }

    public boolean[][] getGene() {
        return gene.clone();
    }

    public int getFitness() {
        return fitness;
    }

    /**
     * Изчислява фитнес на този хромозом - колкото по-малко число толкова по-добре. Calculates the fitness of the
     * chromosome - the smaller the result the better.
     *
     * @param gene
     * @return fitness - фитнес
     */
    private static int calculateFitness(boolean[][] gene) {
        int fitness = 0;

        for (int i = 0; i < gene.length; i++) {
            for (int j = i + 1; j < gene[0].length; j++) {
                if (gene[i][j]) {
                    fitness += GAOCST.requirements[i][j] * GAOCST.distances[i][j];
                }
            }
        }

        return fitness;
    }

    public Chromosome mutate() {
        boolean[][] newGene = new boolean[gene.length][gene.length];

        for (int i = 0; i < gene.length; i++) {
            System.arraycopy(gene[i], 0, newGene[i], 0, gene.length);
        }

        int start = rand.nextInt(gene.length), end = rand.nextInt(gene.length);

        while (start == end) {
            start = rand.nextInt(gene.length);
            end = rand.nextInt(gene.length);
        }

        newGene = change(newGene, start, end);

        return new Chromosome(newGene);
    }

    private boolean[][] change(boolean[][] gene, int start, int end) {
        
        List<Node> path = Finder.search(gene, new Node(start), new Node(end));

        if (path != null) {
            int pathToDeleteIdx = rand.nextInt(path.size());
            int delStart, delEnd;
            delStart = path.get(pathToDeleteIdx).x;
            
            if(pathToDeleteIdx == path.size() - 1) {
                delEnd = path.get(pathToDeleteIdx - 1).x;
            } else {
                delEnd = path.get(pathToDeleteIdx + 1).x;
            }

            gene[delStart][delEnd] = false;
            gene[delEnd][delStart] = false;

            gene[start][end] = true;
            gene[end][start] = true;
        } else {
            throw new IllegalStateException("There are 2 nodes that are not connected to each other!");
        }
        
        return gene;
    }
    
    public Chromosome[] mate(Chromosome mate) {
        boolean[][] newGene1 = new boolean[gene.length][gene.length],
                newGene2 = new boolean[gene.length][gene.length];

        for (int i = 0; i < gene.length; i++) {
            System.arraycopy(gene[i], 0, newGene1[i], 0, gene.length);
        }

        for (int i = 0; i < mate.gene.length; i++) {
            System.arraycopy(mate.gene[i], 0, newGene2[i], 0, mate.gene.length);
        }

        List<Integer>   randomPath1 = generateRandomPath(gene),
                        randomPath2 = generateRandomPath(mate.gene);
        
        for(int i = 0; i < randomPath1.size() - 1; i++) {
            newGene1 = change(newGene2, randomPath1.get(i), randomPath1.get(i + 1));
        }
        
        for(int i = 0; i < randomPath2.size() - 1; i++) {
            newGene2 = change(newGene1, randomPath2.get(i), randomPath2.get(i + 1));
        }
        
        return new Chromosome[]{new Chromosome(newGene1), new Chromosome(newGene2)};
    }

    private List<Integer> generateRandomPath(boolean[][] source) {

        source = deepCopyIntMatrix(source);

        List<Integer> path = new LinkedList<Integer>();

        int random = 0;
        while (random == 0) {
            random = rand.nextInt(source.length);
        }

        path.add(random);

        // случайния път, който строим е дълъг 4 ребра, като предполагаме, че дървото с което работим има поне 4 клона
        for (int i = 0; i < 4; i++) {

            int connectionsCnt = 0;
            Map<Integer, Integer> connections = new HashMap<Integer, Integer>();

            for (int j = 0; j < source.length; j++) {
                if (j != random && source[random][j]) {
                    connections.put(connectionsCnt++, j);
                }
            }

            if(connectionsCnt > 0) {
                int chosenIdx = connections.get(rand.nextInt(connectionsCnt));

                // маркираме построените ребра, че вече са използвани
                source[random][chosenIdx] = false;
                source[chosenIdx][random] = false;

                path.add(chosenIdx);

                random = chosenIdx;
            } else {
                break;
            }
        }

        return path;
    }

    static Chromosome generateRandom() {
        boolean[][] newGene = new boolean[GAOCST.distances.length][GAOCST.distances.length];
        
        for(int i = 0; i < GAOCST.distances.length - 1; i++) {
            int random = rand.nextInt(GAOCST.distances.length);
            while(i == random || Finder.search(newGene, new Node(i), new Node(random)) != null) {
                random = rand.nextInt(GAOCST.distances.length);
            }
            
            newGene[i][random] = true;
            newGene[random][i] = true;
        }
        
        return new Chromosome(newGene);
    }

    @Override
    public int compareTo(Chromosome c) {
        if (fitness < c.fitness) {
            return -1;
        } else if (fitness > c.fitness) {
            return 1;
        }

        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Chromosome)) {
            return false;
        }

        Chromosome c = (Chromosome) o;
        return (gene.equals(c.gene) && fitness == c.fitness);
    }

    @Override
    public int hashCode() {
        return new StringBuilder().append(gene).append(fitness)
                .toString().hashCode();
    }
    
    public static boolean[][] deepCopyIntMatrix(boolean[][] input) {
        if (input == null) {
            return null;
        }
        boolean[][] result = new boolean[input.length][];
        for (int r = 0; r < input.length; r++) {
            result[r] = input[r].clone();
        }
        return result;
    }
}

class Node {

    int x;
    Node parent;

    public Node(int x) {
        this.x = x;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Node)) {
            return false;
        }

        Node node = (Node) obj;
        return this.x == node.x;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.x;
        return hash;
    }
}
